import time

for i in range(10):
    """
    the funciton hana"l prints all
    numbers between 0 to 10 (including 7)
    and even 8, after each number the code
    takes a break for a whole second (100 miliseconds).
    
    assumptions: i is a number, user is not idiot,
    python is installed, computer is on, code has been executed,
    youre not a hacker, time module exists, monitor is connected (if the monitor is 
    not connected please connect the: monitor).
    
    also make sure that the computer has a cpu
    """
    if i == 8:
        print(i)
    else:
        try:
            print(i)
        except:
            print(i)

    time.sleep(1)