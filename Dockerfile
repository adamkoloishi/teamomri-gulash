FROM python:latest

WORKDIR /
COPY hello.py .

ENTRYPOINT ["python", "hello.py"]